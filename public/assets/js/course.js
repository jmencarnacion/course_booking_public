// console.log('Js file')

//lets identify which course this page needs to display inside the browser

// so earlier we passed the course id data inside the parameters of the url for us to quickly identify which course to display

//how are we going to get the value passed inside the url parameters?

//easy.. the answer is use a URLSearchParams() -> this constructor creates and returns a URLSearchParams object.

//window.location -> returns a location object with information about the "current" locattion of the document
//.search -> contains the query string section of the current url.
let urlValues = new URLSearchParams(window.location.search)

//lets check what the structure of this variable will look like
// console.log(urlValues) //checker

//let get only the desired data from the object using a get() method
let id = urlValues.get('courseId')
// console.log(id) //checker
let loggedInNav = document.querySelector("#mainNav");
//lets capture the value of the access token inside the local storage
let token = localStorage.getItem('token')
// console.log(token)
const isUserAdmin = localStorage.getItem("isAdmin")
//lets set firstt a container for all the details that we would want to display
let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let desc = document.querySelector("#courseDesc")
let enroll = document.querySelector("#enrollmentContainer")


//lets send a request going to the endpoint that will allow us to get and display the course details





/*test*/


fetch(`https://fierce-island-38570.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
	// console.log(convertedData._id)
		let idOfTheCourse = convertedData._id
		name.innerHTML = convertedData.name
		price.innerHTML = convertedData.price
		desc.innerHTML = convertedData.description
		enroll.innerHTML = 
		` 
		<a id="enrollButton" class="btn btn-info text-white">Enroll</a>	

		`
		document.querySelector("#enrollButton").addEventListener("click", () => {
				fetch('https://fierce-island-38570.herokuapp.com/api/users/details', {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
				}).then(res => res.json())
				.then(jsonData => {

					let enrollmentsArr = jsonData.enrollments
					let tempArr = []
					// console.log(idOfTheCourse)
					
					enrollmentsArr.map(coursesEnrolled => {
						let coursesEnrolledId = coursesEnrolled.courseId
						tempArr.push(coursesEnrolledId)
						console.log(tempArr)
					}).join("") //end of coursesEnrolled arrow notation

					if (tempArr.indexOf(idOfTheCourse) === -1) {
						console.log('Can still enroll')
						console.log(tempArr)

						fetch(`https://fierce-island-38570.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
							console.log(convertedData)
								name.innerHTML = convertedData.name
								price.innerHTML = convertedData.price
								desc.innerHTML = convertedData.description
								enroll.innerHTML = 
								` 
								<a id="enrollButton" class="btn btn-info text-white">Enroll</a>	

								`
								document.querySelector("#enrollButton").addEventListener("click", () =>{
									
									fetch('https://fierce-island-38570.herokuapp.com/api/users/enroll', {
										method: 'POST',
										headers: {
											'Content-Type': 'application/json',
											'Authorization': `Bearer ${token}` //we have to get the actual value of the token variable
										},
										body: JSON.stringify({
											courseId: id
										})
									}).then(res => {
										return res.json()
									}).then(convertedResponse => {
										console.log(convertedResponse)
										if (convertedResponse === true) {
											alert('Enrollment Successful')

											window.location.replace('./courses.html')
										} else {
											Swal.fire('Enrollment unsuccessful')
										}
									})//end of fetch for enroll
								})//end of addeventlistener

						}) //end of fetch for courseUrl
					} else {
						console.log('cannot')
						Swal.fire({
							icon: 'error',
							title: 'Already Enrolled'
						})
					} // end of else
				})//end of fetch userDetails/JsonData arrow notation
		})//end of addeventlistener
})//end of fetch for courseURL

// // console.log("test")

// //let's identify which course this page needs to display inside the browser

// //so earlier we passed the course id data inside the parameters of the url for us to quickly identify which course to display.

// //how are we going to get the value passed inside the URL params?

// //easy... the answer is we use a URLSearchParams() => this method/constructor creates and returns a URLSearchParams object.

// //window.location -> returns a location object with information about the "current" location of the document
// // .search -> contains the queary string section of the current URL.
// let urlValues = new URLSearchParams(window.location.search)

// //let's check what the structure of this variable will look like
// // console.log(urlValues) // -> this is just a checker

// //let's get only the desired data from the object using a get()
// let id = urlValues.get('courseId')
// // console.log(id)

// //capture the value of the access token inside the local storage
// let token = localStorage.getItem('token')
// // console.log(token) -> this is just a checker

// //let's set first a container for all teh details that we would want to display
// let name = document.querySelector("#courseName")
// let price = document.querySelector("#coursePrice")
// let desc = document.querySelector("#courseDesc")
// let enroll = document.querySelector("#enrollmentContainer")

// //let's send a request going to the endpoint that will allow us to get and display the course details

// fetch('https://fierce-island-38570.herokuapp.com/api/users/details',{
// 		method: 'GET',
// 		headers: {
// 			'Content-Type': 'application/json',
// 			'Authorization': `Bearer ${token}`
// 		}
// 		}).then(res => res.json()).then(jsonData => {
// 			// console.log(jsonData)
// 		const userEnroll = jsonData.enrollments.map(enroll => {
// 			console.log(enroll)
// 		})

// 	fetch(`https://fierce-island-38570.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
// 	console.log(convertedData._id)

// 	name.innerHTML = 	convertedData.name
// 	price.innerHTML =  convertedData.price
// 	desc.innerHTML =  convertedData.description
// 	enroll.innerHTML = `
// 		<a id="enrollButton" class="btn btn-info text-white">enroll
// 		</a>`
// 		document.querySelector("#enrollButton").addEventListener("click", () => {
// 			//insert the course inside the enrollments array of the user



// 		})
// 	})
// });


//////////////ORIGINAL CODE

// console.log("test")

//let's identify which course this page needs to display inside the browser

//so earlier we passed the course id data inside the parameters of the url for us to quickly identify which course to display.

//how are we going to get the value passed inside the URL params?

//easy... the answer is we use a URLSearchParams() => this method/constructor creates and returns a URLSearchParams object.

//window.location -> returns a location object with information about the "current" location of the document
// .search -> contains the queary string section of the current URL.
// let urlValues = new URLSearchParams(window.location.search)

// //let's check what the structure of this variable will look like
// // console.log(urlValues) // -> this is just a checker

// //let's get only the desired data from the object using a get()
// let id = urlValues.get('courseId')
// // console.log(id)

// //capture the value of the access token inside the local storage
// let token = localStorage.getItem('token')
// // console.log(token) -> this is just a checker

// //let's set first a container for all teh details that we would want to display
// let name = document.querySelector("#courseName")
// let price = document.querySelector("#coursePrice")
// let desc = document.querySelector("#courseDesc")
// let enroll = document.querySelector("#enrollmentContainer")

// //let's send a request going to the endpoint that will allow us to get and display the course details
// fetch(`https://fierce-island-38570.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
// 	console.log(convertedData)

// 	name.innerHTML = 	convertedData.name
// 	price.innerHTML =  convertedData.price
// 	desc.innerHTML =  convertedData.description
// 	enroll.innerHTML = `
// 		<a id="enrollButton" class="btn btn-info text-white">enroll
// 		</a>`

// 	document.querySelector("#enrollButton").addEventListener("click", () => {
// 		//insert the course inside the enrollments array of the user
// 		fetch('https://fierce-island-38570.herokuapp.com/api/users/enroll', {
// 			method:'POST',
// 			headers: {
// 				'Content-Type': 'application/json',
// 				'Authorization': `Bearer ${token}` //we have to get the actual value of the token variable
// 			},
// 			body: JSON.stringify({
// 				courseId: id
// 			})
// 		}).then(res => {
// 			return res.json()
// 			console.log(res.json())
// 		}).then(convertedResponse => {
// 			console.log(convertedResponse)
// 			//let's create a control structure that will determine a response according to the result that will be displayed to the client
// 			if (convertedResponse === true) {
// 				Swal.fire({
// 						icon: 'success',
// 						title: 'nice!',
// 						text: 'you have been enrolled!'
// 						})
// 						// .then(loginAlert => {
// 						//     window.location.replace('./courses.html');
// 						// })
// 			} else {
// 				Swal.fire({
// 						icon: 'error',
// 						title: 'oh no!',
// 						text: 'enrollment failed, please try again'
// 						})
			
// 			}
// 		})
// 	})
// })


// //logic that will prevent the user in enrolling the same user twice
