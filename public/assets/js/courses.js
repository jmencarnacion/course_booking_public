//what will be the first task in retrieving all records insdie the course collection
let adminControls = document.querySelector('#adminButton')
//target the container from the html document.
let container = document.querySelector('#coursesContainer')

//let's determine if there is a user currently logged in
//let's capture one of the properties that is currently stored in our web storage
const isAdmin = localStorage.getItem('isAdmin') 
//let's create a control structure to determine the display in the front end
if (isAdmin == "false" || !isAdmin) {
	
	adminControls.innerHTML = null;
} else {
	adminControls.innerHTML = `
		<div class="col-md-2 offset-md-10"><a href="./addCourse.html" class="btn btn-block btn-info">add course</a></div>
	`
}



//let's send a request to retrieve all documents from the courses collection


//START OF STRETCH GOAL
// if (isAdmin == "false" || !isAdmin) {

// 	fetch('http://localhost:4000/api/courses/').then(res => res.json()).then(jsonData => {
// 	console.log(jsonData)// we only inserted this as a checker

// 	//let's declare a variable that will display the result in the browser depending on the return.
// 	let courseData;
// 	//create a control structure that will determine the value that the variable will hold

// 	if (jsonData.length < 1) {
// 		courseData = "no courses available"
// 		console.log("no courses available")
// 		container.innerHTML = courseData
// 	} else {
// 		//if the condition given is not met, we will display the contents of the array inside our page

// 		//we will iterate the courses collection and display each course inside the browser
// 		courseData = jsonData.map(course => {
// 			//let's check the make up/structure of each element inside the courses collection
// 			console.log(course);
// 			//let's use template literals to pass or inject the properties of our object inside each section of a card component
			
// 			//let's fix the current behavior of our course page that it can only display the last object inserted inside the array.
// 			//so fat we did not indicate what would be the return of our map()
// 			return(
// 					`
// 						<div class="col-md-6 my-3">
// 							<div class="card">
// 								<div class="card-body">
// 									<h4 class="card-title text-left">
// 										course name: ${course.name}
// 									</h4>
// 									<p class="card-text text-left">price: ${course.price}</p>
// 									<p class="card-text text-left">description: ${course.description}</p>
// 									<p class="card-text text-left">created on: ${course.createdOn}</p>
// 								</div>
// 								<div class="card-footer">
// 									<a href="#" class="btn btn-info">view course details
// 									</a>
// 								</div>
// 							</div>
// 						</div>
// 					` //you can think of a better message / for demonstration purposes
// 				)
// 		}).join("")//we will use this .join() to create a return of a new string/it concatenated
// 		container.innerHTML = courseData;
// 		}
// 	})

// 	} else {

// 	fetch('http://localhost:4000/api/courses/').then(res => res.json()).then(jsonData => {
// 	console.log(jsonData)// we only inserted this as a checker

// 	//let's declare a variable that will display the result in the browser depending on the return.
// 	let courseData;
// 	//create a control structure that will determine the value that the variable will hold

// 	if (jsonData.length < 1) {
// 		courseData = "no courses available"
// 		console.log("no courses available")
// 		container.innerHTML = courseData
// 	} else {
// 		//if the condition given is not met, we will display the contents of the array inside our page

// 		//we will iterate the courses collection and display each course inside the browser
// 		courseData = jsonData.map(course => {
// 			//let's check the make up/structure of each element inside the courses collection
// 			console.log(course);
// 			//let's use template literals to pass or inject the properties of our object inside each section of a card component
			
// 			//let's fix the current behavior of our course page that it can only display the last object inserted inside the array.
// 			//so fat we did not indicate what would be the return of our map()
// 			return(
// 					`
// 						<div class="col-md-6 my-3">
// 							<div class="card">
// 								<div class="card-body">
// 									<h4 class="card-title text-left">
// 										course name: ${course.name}
// 									</h4>
// 									<p class="card-text text-left">price: ${course.price}</p>
// 									<p class="card-text text-left">description: ${course.description}</p>
// 									<p class="card-text text-left">created on: ${course.createdOn}</p>
// 								</div>
// 								<div class="card-footer">
// 									<a href="#" class="btn btn-info">edit
// 									</a>
// 									<a href="#" class="btn btn-info">delete
// 									</a>
// 								</div>
// 							</div>
// 						</div>
// 					` //you can think of a better message / for demonstration purposes
// 				)
// 		}).join("")//we will use this .join() to create a return of a new string/it concatenated
// 		container.innerHTML = courseData;
// 	}
// })


// }

fetch('https://fierce-island-38570.herokuapp.com/api/courses/').then(res => res.json()).then(jsonData => {
	console.log(jsonData)// we only inserted this as a checker

	//let's declare a variable that will display the result in the browser depending on the return.
	let courseData;
	//create a control structure that will determine the value that the variable will hold

	if (jsonData.length < 1) {
		courseData = "no courses available"
		console.log("no courses available")
		container.innerHTML = courseData
	} else {
		//if the condition given is not met, we will display the contents of the array inside our page

		//we will iterate the courses collection and display each course inside the browser
		courseData = jsonData.map(course => {
			//let's check the make up/structure of each element inside the courses collection
			console.log(course._id);
			console.log(course.name)
			//let's use template literals to pass or inject the properties of our object inside each section of a card component
			
			//this is the answer key for the task
			let cardFooter;
			//create a control structure that will evaluate the role of the user
			if (isAdmin == "false" || !isAdmin) {
				cardFooter= 
				`<a href="./course.html?courseId=${course._id}" class="btn btn-info">view course details
				</a>
				`
			} else {
				cardFooter =
				`
				<a href="" class="btn btn-info text-white">edit course
				</a>
				<a href="" class="btn btn-info text-white">delete course
				</a>
				`
			}

			//let's fix the current behavior of our course page that it can only display the last object inserted inside the array.
			//so fat we did not indicate what would be the return of our map()
			return(
					`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title text-left">
										course name: ${course.name}
									</h4>
									<p class="card-text text-left">price: ${course.price}</p>
									<p class="card-text text-left">description: ${course.description}</p>
									<p class="card-text text-left">created on: ${course.createdOn}</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					` //you can think of a better message / for demonstration purposes
				)
		}).join("")//we will use this .join() to create a return of a new string/it concatenated
		container.innerHTML = courseData;
	}
})

	// `
	// <div class="col-md-6 my-3">
	// 	<div class="card">
	// 		<div class="card-body">
	// 			<h3 class="card-title">Course Name</h3>
	// 			<p class="card-text text-left">Description</p>
	// 			<p class="card-text text-left">Price</p>
	// 			<p class="card-text text-left">Created On</p>
	// 		</div>
	// 	</div>
	// </div>
	// `