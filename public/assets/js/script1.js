console.log("indextest")

let navindexItems =  document.querySelector("#navSession1")
let userToken1 = localStorage.getItem("token")

//let's create a control structure that will determine the display inside the navbar if there is a user currently logged in the app

  if(!userToken1) {
	navindexItems.innerHTML = 
				`	
					<li class="nav-item"><a href="./pages/login.html" class="nav-link">login</a></li>
				`
} else {
	navindexItems.innerHTML = 
				`
					<li class="nav-item"><a href="./pages/courses.html" class="nav-link">profile</a></li>
					<li class="nav-item"><a href="./pages/logout.html" class="nav-link">logout</a></li>
				`
}