
//console.log("Hello from JS file");

//let's target first our form component and place it inside a new variable
let registerForm = document.querySelector("#registerUser");

//inside the first parameter of the method describe the event that it will listen to, while inside the 2nd parametere let's describe the action / procedure that will happen upon triggering the said event
registerForm.addEventListener("submit", (event) => {
	event.preventDefault() //to avoid page refresh/page redirection once that the said event has been triggered.

	//capture each values inside the input fields first, then repackage them inside a new variable
	let fName = document.querySelector("#firstName").value
	//let's create a checker to make sure that we are successful in capturing the values.
	//console.log(firstName)
	let lastName = document.querySelector("#lastName").value
	//let's create a checker to make sure that we are successful in capturing the values.
	//console.log(lastName)
	let email = document.querySelector("#userEmail").value
	//let's create a checker to make sure that we are successful in capturing the values.
	//console.log()
	let mobileNo = document.querySelector("#mobileNum").value
	//let's create a checker to make sure that we are successful in capturing the values.
	//console.log(mobileNo)
	let password = document.querySelector("#password1").value
	//let's create a checker to make sure that we are successful in capturing the values.
	//console.log(password)
	let verifyPassword = document.querySelector("#password2").value
	//let's create a checker to make sure that we are successful in capturing the values.
	//console.log(verifyPassword)


	//let's create a data validation for our register page
	//why do we need to validate data? to check and verify if the data that we will accept is accurate
	//we do data validation to make sure that the storage space will be properly utilized

	//the following info/data that we can validate
	//email, password. mobileNo
	//let's create a control structure to determine the next set of procedures before the user can register a new account.
	//it will be a lot efficient for you to sanitize the data before submitting it to the backend/server for processing

	//stretch goals: (these are not required)
	// => the password should contain both alphanumeric characters and at least 1 special character 

	var regularExpression  = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/;

	if((fName !== "" && lastName !== "" && email !== "" && password !== "" && verifyPassword!== "" ) && (password === verifyPassword) && (mobileNo.length === 11))
	{
		if (!regularExpression.test(password)) {
			Swal.fire({
							icon: 'error',
							title: 'oops!',
			  				text: 'please consider a stronger password!'
					})
		} else {
			//how are we going to integrate our email-exists method?
		//we are going to send out a new request
		//before you allow a user to create a new account, check if the email value is still available for us. this will ensure that each user will have their unique user email
		//upon sending this request, we are instanciating a promise object, which means we should apply a method to handle the response
		fetch('https://fierce-island-38570.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		}).then(res => {
			return res.json() // to make it readable once the response returns to the client side
		}).then(convertedData => {
			//what would the response look like?
			//let's create a control structure to determine the proper procedure depending on the response
			if (convertedData === false) {
				//let's allow the user to register an account.
				
				//this block of code will run if the condition has been met
				//how can we create a new account for user using the data that he/she entered? answer: using fetch request!
				//the first parameter in fetch(url,[options]) describes the destination of the request
				fetch('https://fierce-island-38570.herokuapp.com/api/users/register',{
					//we will now describe the structure of our request for register
					method:'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					//API only accepts request in a string format
					body: JSON.stringify({ 
						firstName: fName,
						lastName:lastName,
						email: email,
						mobileNo: mobileNo,
						password: password
					}) //properties from userSchema, JSON.stringify allows body to be converted to JSON string format
				}).then(res => {

					console.log(res)
					// console.log("hello");
					return res.json()
					// console.log(res)
				}).then(data => {
					console.log(data)
					//let's create a control structure to give out a proper response depending on the return from the backend.
					if(data === true) {
							Swal.fire({
									icon: 'success',
									title: 'awesome!',
									text: '	account has been created'
									}).then(loginAlert => {
									window.location.replace('./login.html')
							});
					} else {
						//inform the user that something went wrong
						Swal.fire({
							icon: 'error',
							title: 'OH NO!',
			  				text: 'Something went wrong during registration!'
						})
					}
				})
			} else {
				//let's inform the user what went wrong
				Swal.fire({
				icon: 'warning',
				title: 'OH NO!',
  				text: 'email already exists!'
				})
			}
		})
		}
	} else {
		Swal.fire({
					icon: 'error',
					title: 'oops!',
			  		text: 'please check your input'
				})
	}
})

	//let's create a simple alert message just to inform the user of a response.
	//alert("succesfully captured data")
	//the .fire() will allow you to trigger the pop-up box
	//what if I want to create different sections of my pop up box
/*	Swal.fire(
		{
			icon: "success",
			title: "YAS KWEEN!",
			text: "successfully registered"
		}
	) //sweet alert can only accept strings
*/