//get the value of the access token inside the local storage and place it inside a new variable using getItem method
let token = localStorage.getItem("token")
// console.log(token)
let isAdmin = localStorage.getItem("isAdmin")

let profContainer = document.querySelector("#profileContainer")
//out goal here is to display the information about the user details
//send a request to the backend project

if (!token) {
	Swal.fire({
				icon: 'warning',
				title: 'hold up a minute!',
				text: 'please login first'
				}).then(loginAlert => {
				window.location.replace('./login.html');
				});
} else {
	fetch('https://fierce-island-38570.herokuapp.com/api/users/details',{
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
	}).then(res => res.json()) //shortened .then() syntax
	.then(jsonData => {
	//let's create a checker to make sure the fetch is successful
	// console.log(jsonData)
	console.log(jsonData.enrollments)
	const userSubjects = jsonData.enrollments.map(subject => {
		console.log(subject)
		//create a container that will serve as the html boilerplate that will diusplay the properties of the subject that we want to get
		fetch(`https://fierce-island-38570.herokuapp.com/api/courses/${subject.courseId}`, {
		}).then(res => res.json()).then(convertedData => {
			  console.log(convertedData)
		// const userDetails = convertedData.map(details=> {
		//  console.log(details)
		// })


		//const obj1= JSON.parse(userSubjects)
		//const obj2= JSON.parse(userDetails)

		const mergedObject = {...subject,...convertedData}
		console.log(mergedObject)

		//return (
		document.getElementById('container').innerHTML +=`
			<tr>
				<td> ${mergedObject.name}</td>
				<td> ${mergedObject.description}</td>
				<td> ${mergedObject.enrolledOn}</td>
				<td> ${mergedObject.status}</td>
			</tr>
		`
		//)
	})

}).join("")
	
	//how are we going to display the information inside the front end component?
	//let's target the div element first using it's id attribute
	//let's create as section to display all the courses the user is enrolled in 
	//get all the elements inside the enrollment array

	profContainer.innerHTML = `
		<div class="col-md-12" id="profContainer">
			<section class="jumbotron my-5 bg-light">
				<h2 class="text-center">welcome</h2>
				<h4 class="text-center">first name: ${jsonData.firstName}</h3>
				<h4 class="text-center">last name: ${jsonData.lastName} </h3>
				<h6 class="text-center">email: ${jsonData.email}</h3>
				<h6 class="text-center">mobile number: ${jsonData.mobileNo}</h3>
				<table class="table">
					<tr>
						<th>course name: </th>
						<th>course description: </th>
						<th>enrolled On: </th>
						<th>status: </th>
						<tbody id="container"></tbody>	
					</tr>
				</table>
			</section>
		</div>
		`
	})
}

if (isAdmin == "true") {
	alert("hello admin!")
	window.location.replace('./addCourse.html')
}



// fetch('http://localhost:4000/api/users/details',{
// 	method: 'GET',
// 	headers: {
// 		'Content-Type': 'application/json',
// 		'Authorization': `Bearer ${token}`
// 	}
// }).then(res => res.json()) //shortened .then() syntax
// .then(jasonData => {
// 	//let's create a checker to make sure the fetch is successful
// 	console.log(jasonData)
// 	//how are we going to display the information inside the front end component?
// 	//let's target the div element first using it's id attribute
// 	//let's create as section to display all the courses the user is enrolled in 
// 	profContainer.innerHTML = `
// 		<div class="col-md-12" id="profContainer">
// 			<section class="jumbotron my-5 bg-light">
// 				<h4 class="text-center">First Name: ${jasonData.firstName}</h3>
// 				<h4 class="text-center">Last Name:${jasonData.lastName} </h3>
// 				<h6 class="text-center">Email:${jasonData.email}</h3>
// 				<h6 class="text-center">Mobile Number:${jasonData.mobileNo}</h3>
// 				<table class="table">
// 					<tr>
// 						<th>Course ID: </th>
// 						<th>Enrolled On: </th>
// 						<th>Status: </th>
// 						<tbody></tbody>
// 					</tr>
// 				</table>
// 			</section>
// 		</div>
// 	`
// })
