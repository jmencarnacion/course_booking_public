//Navbar Script

//for pages, we captured the navsession element from the navbar component
let navItems =  document.querySelector("#navSession")
// console.log(navItems)
let userToken = localStorage.getItem("token")
// console.log(userToken)

//let's create a control structure that will determine the display inside the navbar if there is a user currently logged in the app

  if(!userToken) {
	navItems.innerHTML = 
				`	
					<li class="nav-item"><a href="./login.html" class="nav-link">login</a></li>
				`
} else {
	navItems.innerHTML = 
				`
					<li class="nav-item"><a href="./logout.html" class="nav-link">logout</a></li>
				`
}

//for index
// let navIndexItems =  document.querySelector("#navIndexSession")
// // console.log(navItems)
// // let userToken = localStorage.getItem("token")
// // console.log(userToken)

//   if(!userToken) {
// 	navIndexItems.innerHTML = 
// 				`
// 						<li class="nav-item"><a href="./pages/register.html" class="nav-link">register</a></li>
// 						<li class="nav-item"><a href="./pages/login.html" class="nav-link">login</a></li>
// 				`
// } else {
// 	navIndexItems.innerHTML = `
// 						<li class="nav-item"><a href="./pages/logout.html" class="nav-link"> logout </a></li>
// 	`
// }

// console.log("test")