// console.log("Hello from JS")

//let's target our form component inside our document
let loginForm = document.querySelector('#loginUser')

//the login form will be used by the client to insert his/her account to be authenticated by the app.
loginForm.addEventListener("submit", (enter) => {
	enter.preventDefault() //prevents page redirection

	//let's capture the values of our form components
	let email = document.querySelector("#userEmail").value
	let pass = document.querySelector("#password").value

	//let's create a checker to confirm the acquired values.
		// console.log(email)
		// console.log(pass)

	//our next task is to validate the data inside the input fields first.
	if (email == "" || pass == "") {
		Swal.fire({
						icon: 'error',
						title: 'oops!!',
						text: 'please provide your email and/or password'
				})
	} else {
		//send a request to the desired endpoint
		fetch("https://fierce-island-38570.herokuapp.com/api/users/login",{
			method:'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email,
				password:pass
			})
		}).then(res => {
			return res.json() //we are instantiating a promise (resolve, reject)
		}).then(dataConverted => {
			//check if the conversion of the response has been successful



			// console.log(dataConverted)
			//save the generated access token inside the local storage property of the browser
			if (dataConverted.accessToken) {
				localStorage.setItem('token',dataConverted.accessToken)
				// alert("successfully generated access token"); we just created this as confirmation of the previous task.

				//make sure that before you redirect the user to the next location, you have to identify the access rights that you want to grantfor that user.
				//how can we know if the user is an admin or not?
				fetch('https://fierce-island-38570.herokuapp.com/api/users/details',{
					headers: {
						//let's pass on the value of our access token
						'Authorization': `Bearer ${dataConverted.accessToken}`
					} //upon sending this request we are instantiating a promise that can lead to 2 possible outcomes. what do we need to handle the possible outcome states of this promise?
				}).then(res => {
					return res.json()
				}).then(data => {
					//check if we are able to get the user's data
					// console.log(data);
					//fetching the user details/info is a success
					//save the "id", "isAdmin"
					localStorage.setItem('id', data._id)
					localStorage.setItem('isAdmin', data.isAdmin)
					//as developers its up to you if you want to create a checker to make sure that all values are saved properly inside the web storage.
					// console.log('items are set inside the local storage')
					window.location.replace('./profile.html')
				})
				//how would you redirect the user to another location in your app?
				
			} else {
				//this block of code will run if no access token was generated
				Swal.fire({
						icon: 'error',
						title: 'oops!!',
						text: 'email and password does not match'
						}).then(loginAlert => {
						    window.location.replace('./login.html');
						});
				// alert("email is not associated with any account")
				// window.location.replace('./login.html')
			}
		})
	}
})
