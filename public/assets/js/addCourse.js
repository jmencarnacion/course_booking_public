// let's create our addCourse page
let formSubmit = document.querySelector('#createCourse');

const isAdmin = localStorage.getItem("isAdmin")

if (isAdmin == "false" || !isAdmin) {
	alert("this page is for admins only")
	window.location.replace('./login.html')
}

//let's  acquire an event that will apply in our form component
//create a subfunction inside the method to describe the procedure or the action that will take place upon triggering the event. 
formSubmit.addEventListener("submit", (event) => {
	event.preventDefault() //this will avoid page redirection

	//let's target the values of each component inside the form.
	let name = document.querySelector("#courseName").value
	let cost = document.querySelector("#coursePrice").value
	let desc = document.querySelector("#courseDesc").value

	//let's create a checker to see if we were able to capture the values of each input fields: lines 15 to 17
	// console.log(name)
	// console.log(cost)
	// console.log(desc)

	//send a request to the backend project to process the data for creating a new entry inside our courses collection

	if ((name !== "" && desc !== "" && cost !== "")) {

		//this will integrate if an course-exist
		fetch('https://fierce-island-38570.herokuapp.com/api/courses/course-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name:name
			})
		}).then(res => {
			return res.json()
		}).then(convertedCourseData => {
			if (convertedCourseData === false) {
				//upon creating this fetch api request we are instantiating a promise
				fetch('https://fierce-island-38570.herokuapp.com/api/courses/create', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
					//what are the properties of the document that the user needs to fill?
					name: name, 
					description: desc, //description came from models schema and desc came from declared value above.
					price: cost
					})
				}).then(res => {
				// console.log(res)
				return res.json(); //why do we need to convert the response into a json format?
				}).then(info => {
				// console.log(info)
				//let's create a control structure that will describe the response of the UI to the client.

				if (info === true) {
					Swal.fire({
						icon: 'success',
						title: 'nice!',
						text: 'course created successfully!'
					})
				} else {
					Swal.fire({
						icon: 'error',
						title: 'oops!',
						text: 'failed to create a new course!'
					})
				}
				//let's check what the response looks like when JSON method is applied | it will display true or false based on promise on controller
		}) 
			} else {
				Swal.fire({
					icon: 'warning',
					title: 'wait a sec!',
	  				text: 'course already exists!'
				})
			}
		})
	//how can we handle the promise object that will be returned once the fetch method event has happened
	} else {
		Swal.fire({
				icon: 'error',
				title: 'oops!',
  				text: 'please fill the details!'
			})
	}
})


// const isAdmin = localStorage.getItem('isAdmin') 
// //let's create a control structure to determine the display in the front end
// if (isAdmin == "false" || !isAdmin) {
	//use this snippet for stretch goal