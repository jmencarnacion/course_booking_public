// console.log("test logout")

//upon logging out the credentials of the user should be removed from the local storage
//how to delete information or data inside the local storage
localStorage.clear();

//the clear method will wipe out/remove the contents of the localStorage

//upon clearing out the local storage, redirect the user back to the login page.

window.location.replace('./login.html')